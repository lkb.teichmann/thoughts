Fermi prices
------------

The concept of a Fermi energy is very important in solid state physics,
especially for the physics of metals and semiconductors. Economics knows a
similar concept of marginal cost, but it is usually used differently.

Here I try to apply the concept of a Fermi energy to the economics of
resources. The core concept of economics is the balancing of supply and
demand. For natural resources, like crude oil, the supply is given by
the known deposits and the available extraction technologies. Different
deposits usually have different cost of extraction. Plotting the possible
extraction quantities over their cost gives us a curve very close to the
density of states in solid state physics, if we exchange price with energy.

In economics, the same can be done for the demand. This has no analogy in
solid state physics, as the number of electrons occupying the density of
states is fixed. However, in cold atom physics, experiments with varying
number of atoms in a solid state-like potential have been performed.

As a producer of a resource is trying to maximize profit, it will sell at the
highest achievable price. This automatically determines which resources will 
be extracted, and which will not: the price will rise until all demand is
fulfilled, so all producers not able to produce at this price will shut down
operation, while all others will run operation. This are the occupied or
unoccupied states in solid state physics. The energy of the highest occupied
state is called the Fermi energy, in the same way we can call the cost of the
most expensive producer in operation the Fermi cost. Maximizing profit leads
to the market price being the Fermi cost, so we can call it the *Fermi price*.

Temperature
-----------

We can drive the analogy further. In a given market situation, there will
always be some producers which produce at a higher cost than the Fermi price,
so effectively at a loss, simply because they have not yet have the time to
shut down operation, or other reason, like politics. In the same way cheaper
sources may not produce yet. There will be more or less of those inefficient
producers depending on how volatile a market is. Solid state physics has a
name for this: the amount of unoccupied states below the Fermi energy, or
occupied states above, is the temperature of the solid. This is a nice analogy
to free markets: once a disruption in market happens, the volatility will
increase, but with time the market will approach a steady state. Or in solid
state terms: the market cools down.

Insulators
----------

There are resources where the production cost is vastly different depending
on the extraction method. A standard example is drinking water: while ground
water is very cheap to pump, once it dries up, the next options are usually
transportation from afar, or desalination, which both are very costly. This
is known as a *band gap*. Once the cheap production method is used up, the
Fermi cost suddenly jumps to the cost of the more costly method. In a free
market, so should the price for the resource. In solid state physics, this is
called an insulator, while the above case of a smooth price increase with
demand is a conductor.

Traditionally economics is very bad at treating such insulator-resources. It
is often considered "unfair" that a small change in demand should have such
a drastic effect on the price. The case of drinking water is a classic
example: in a dry summer, why should the average household suddenly have to
pay much more, wouldn't it be much easier if those rich people would stop
filling their pools? But the counter-argument is actually also not false:
the pool owner might be fully willing to pay the higher price, and compared
to his private jet, the economic and ecologic impact is only small.

While this is not the point of this essay, I still want to do a little
excursion on this problem. The solution is actually fairly simple: every
consumer should have the right to obtain his fair share of the resource at
the low price, while over-consumers should just pay the higher one. So
the average family should just get their typical share of drinking water
at the price to pump ground water, while the additional water needed to
fill the pool should be priced at the cost of, say, desalination.

Bosons
------

Particles which show the behavior described above, in this case electrons,
are generally called *Fermions*, as they follow Fermi-Dirac-Statistics, which
is the concept described above. However, physics knows another class of
particles, which follow a different type of statistcs, the
Bose-Einstein-Statistics. Many of these particles can occupy a single state,
such that the entire concept of a highest occupied energy breaks down, as all
particles may occupy the lowest state.

This is a good analogy for non-physical resources, like interlectual property:
it can be sold as often as one wants, without the need to reproduce it. And
as for physics, the entire concept of a marginal cost breaks down.